package Percobaan2;

public class Cat extends Animal implements Pet{
    private String name;
    
    Cat(String name){
        super(4);
        this.name = name;
    }

    Cat(){
        super(4);
        this.name = " ";
    }
    
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public void play(){
        System.out.println("kucing sedang bermain");
    }

    public void eat(){
        System.out.println("kucing sedang makan");
    }
}
