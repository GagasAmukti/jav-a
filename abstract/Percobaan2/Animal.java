package Percobaan2;
abstract class Animal {
    protected int legs;
    
    Animal(){
        this.legs = 0;
    }
    Animal(int legs){
        this.legs = legs;
    }
    public abstract void eat();
    public void walk(){
        System.out.println("hewan ini bergerak dengan berjalan");
    }
}
