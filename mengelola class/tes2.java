public class tes2 {
    public static void main(String[] args) {
        Mahasiswa2 mahasiswa1 = new Mahasiswa2(312060032, "Gagas");
        Mahasiswa2 mahasiswa2 = new Mahasiswa2("Gagas");
        Mahasiswa2 mahasiswa3 = new Mahasiswa2();
        
        System.out.println("mahasiswa 1 nama : " + mahasiswa1.getNama() + " nrp : " + mahasiswa1.getNrp());
        System.out.println("mahasiswa 2 nama : " + mahasiswa2.getNama() + " nrp : " + mahasiswa2.getNrp());
        System.out.println("mahasiswa 3 nama : " + mahasiswa3.getNama() + " nrp : " + mahasiswa3.getNrp());
    }
}
