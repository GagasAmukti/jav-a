package percobaan4;
import java.util.*;
public class PriorityQueueDemo {
    static PriorityQueue <String> stringQueue;
    public static void main(String[] args) {
        stringQueue= new PriorityQueue<String>();
        stringQueue.add("ab");
        stringQueue.add("abcd");
        stringQueue.add("abc");
        stringQueue.add("a");
        //don't use iterator which may or may not
        //show the PriorityQueue'sorder
        while(stringQueue.size() > 0){
        System.out.println(stringQueue.remove());
        }
    }
}
