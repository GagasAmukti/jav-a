package latihan4;

class Dosen extends Orang{
    public int nip;

    Dosen(String nama){
        super(nama);
        this.nip = 0;
    }
    Dosen(String nama, int nip){
        super(nama);
        this.nip = nip;
    }
    Dosen(String nama, int nip, int umur){
        super(nama,umur);
        this.nip = nip;
    }
    public String INGPO(){
        return"Nama Dosen\t: " + super.nama +
               "\nUmur Dosen\t: " + super.umur +
               "\nNIP Dosen\t: " + this.nip ;
    }
}
