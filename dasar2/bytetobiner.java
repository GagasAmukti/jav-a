import java.util.*;
public class bytetobiner {
    public static void main(String[] args) {
        byte bilangan = 0;
        String biner;
        Scanner masukan = new Scanner(System.in);

        System.out.print("Masukkan Bil Desimal Byte : ");
        bilangan = masukan.nextByte();
        biner = Integer.toBinaryString(bilangan);
        System.out.println("Hasil Byte " + bilangan + " to Biner adalah : " + biner);
        masukan.close();
    }
}
