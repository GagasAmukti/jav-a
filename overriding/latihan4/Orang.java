package latihan4;

class Orang {
    protected String nama;
    protected int umur;

    protected Orang(){
        umur = 0;
        nama = "";
    }
    protected Orang(String nama ){
        umur = 0;
        this.nama = nama;
    }
    protected Orang(String nama, int umur){
        this.umur = umur;
        this.nama = nama;
    }
}
