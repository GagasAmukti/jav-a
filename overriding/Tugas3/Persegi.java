package Tugas3;

class Persegi extends bangunDatar{
    protected int sisi;

    Persegi(int sisi){
        this.sisi = sisi;
    }

    protected double luas(){
        return sisi * sisi;
    }

    protected double keliling(){
        return sisi * 4;
    }

}
