package Tugas1;

public class Bird extends Animal implements Flyer {
    public void takeOff(){
        System.out.println("Bird berhasil take off");
    }

    public void land(){
        System.out.println("Bird berhasil landing"); 
    }

    public void fly(){
        System.out.println("Bird terbang dengan mengepakkan sayap");  
    }
    
    public void buildNest(){
        System.out.println("Bird membangun sarang di pohon");  
    }

    public void layEggs(){
        System.out.println("Bird bereproduksi dengan ber telur");  
    }

    public void eat(){
        System.out.println("burung sedang makan");
    }
}
