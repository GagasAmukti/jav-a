package percobaan10;
import java.util.*;
public class RemoveValueFromHashMapExample {
    public static void main(String[] args) {
        //create HashMapobject
    HashMap hMap= new HashMap();
    //add key value pairs to HashMap
    hMap.put("1","One");
    hMap.put("2","Two");
    hMap.put("3","Three");
    Object obj= hMap.remove("2");
    System.out.println(obj+ " Removed from HashMap");
    hMap.clear();
    System.out.println("Total key value pairs in Hash Map are : " + hMap.size());
    }
}
