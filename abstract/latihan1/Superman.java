package latihan1;

public class Superman implements Flyer{
    public void takeOff(){
        System.out.println("Superman berhasil take off");
    }

    public void land(){
        System.out.println("Superman berhasil landing"); 
    }

    public void fly(){
        System.out.println("Superman terbang dengan kekuatannya");  
    }
}
