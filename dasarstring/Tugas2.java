import java.util.*;
public class Tugas2 {
    public static void main(String[] args) {
        Scanner masukan = new Scanner(System.in);
        int spasi1 = 0, spasi2 = 0;

        System.out.print("masukkan nama 1 : ");
        String nama1 = masukan.nextLine();
        System.out.print("masukkan nama 2 : ");
        String nama2 = masukan.nextLine();

        spasi1 = nama1.indexOf(" ");
        spasi2 = nama2.indexOf(" ");
        
        String family1 = nama1.substring(spasi1,nama1.length());
        String truename1 = nama1.substring(0,spasi1);
        String family2 = nama2.substring(spasi2,nama2.length());
        String truename2 = nama2.substring(0,spasi2);
        
        String hasil1 = truename1 + family2;
        String hasil2 = truename2 + family1;

        System.out.println("HASIL PENUKARAN FAMILY NAME");
        System.out.println("Nama 1 : " + hasil1);
        System.out.println("Nama 2 : " + hasil2);
        masukan.close();
    }
}
