package Tugas2;

public class Katak {
    protected int umur;
    protected String nama;
    
    Katak(){
        this.nama = "";
        this.umur = 0;
    }

    Katak(int umur, String nama){
        this.nama = nama;
        this.umur = umur;
    }

    protected String caraBergerak(String tipe){
        if(tipe == "katak"){
            return "Melompat";
        }else{
            return "Berenang";
        }
    }

    public String INGPOkat(){
        return"\t|" + this.nama + "\t|" + this.umur +
               "\t|" + caraBergerak("katak") + "\t|\t 0\t|";
    }
}
