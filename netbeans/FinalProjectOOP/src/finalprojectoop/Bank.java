/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package finalprojectoop;

import BANK.Nasabah;

/**
 *
 * @author amukt
 */
class Bank {
    protected int jumlahNasabah = 0, i = 0;
    protected Nasabah[] nasabah;

    public Bank(){
        nasabah = new Nasabah[100000];
    }

    public void tambahNasabah(String namaAwal, String namaAkhir, String noRekening, int noId){
        nasabah[noId] = new Nasabah(namaAwal, namaAkhir, noRekening);
    }

    public int getJumlahNasabah(){
        return this.jumlahNasabah;
    }

    public Nasabah getNasabah(int indeks){
        return nasabah[indeks];
    }
}
