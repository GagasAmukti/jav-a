/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package finalproject;
import java.util.*;
import java.io.*;
/**
 *
 * @author amukt
 */
public class MainRun extends Bank{
    public static void main(String[] args) {
        String namaAwal = "Syaugi";
        String namaAkhir ="Arab";
        String rekening = "6567";
        int pilihan, pilihanLogin = 1, tabungan = 500000, tambahUang, jumlahAmbil, temp, iduser = 6768, jumTrans, jumBayar;
        char ulang = 'n', pilihanLogout = 'n';
        boolean status, valid;
        Scanner masukan = new Scanner(System.in);
        
        do{
//            System.out.println("");
//            System.out.println("Selamat datang KeBank GGS ");
//            System.out.println("1. Login");
//            System.out.println("2. Register");
//            System.out.println("3. keluar");
//            System.out.print("Masukkan pilihan anda\t: ");
//            pilihanLogin = masukan.nextInt();
              //masukan.nextLine();

            loginpage login = new loginpage();
            login.setVisible(true);
            
            Bank bank = new Bank();
            
            bank.tambahNasabah(namaAwal, namaAkhir, rekening, iduser);
            bank.getNasabah(iduser).setTabungan(new Tabungan(tabungan));

            if(login.){
                System.out.print("Masukkan Rekening : ");
                rekening = masukan.nextLine();
                System.out.print("Masukkan No id user : ");
                iduser = masukan.nextInt();
                System.out.print(iduser);
                bank.getNasabah(iduser).getNamaAwal();
                masukan.nextLine();
                bank.getNasabah(iduser);
            }

            else if(pilihanLogin == 2){
                System.out.print("Masukkan Nama Awal Anda : ");
                namaAwal = masukan.nextLine();
                System.out.print("Masukkan Nama Akhir Anda : ");
                namaAkhir = masukan.nextLine();
                System.out.print("Masukkan Rekening : ");
                rekening = masukan.nextLine();
                System.out.print("Masukkan No id user : ");
                iduser = masukan.nextInt();
                masukan.nextLine();
                bank.tambahNasabah(namaAwal, namaAkhir, rekening,iduser);
                System.out.print("Masukkan Tabungan Awal : ");
                tabungan = masukan.nextInt();
                bank.getNasabah(iduser).setTabungan(new Tabungan(tabungan));
            }
            else { 
                System.exit(0);
            }
            do{
                System.out.println("\nSelamat Datang " + bank.getNasabah(iduser).getNamaAwal() + " " + bank.getNasabah(iduser).getNamaAkhir());
                System.out.println("Pilihan Menu Bank");
                System.out.println("1. Cek Saldo ");
                System.out.println("2. Tambah Saldo");
                System.out.println("3. Ambil Saldo ");
                System.out.println("4. Transfer Saldo ");
                System.out.println("5. Pembayaran ");
                System.out.println("6. Keluar ");
                System.out.print("Masukkan Pilihan Menu : ");
                pilihan = masukan.nextInt();
                masukan.nextLine();

                if(pilihan == 1){
                    temp = bank.getNasabah(iduser).getTabungan().getSaldo();
                    System.out.println("Saldo anda adalah (dalam IDR) : " + temp);
                }
                
                else if(pilihan == 2){
                    System.out.print("Berapa jumlah saldo yang mau ditambah  : ");
                    tambahUang = masukan.nextInt();
                    bank.getNasabah(iduser).getTabungan().simpanUang(tambahUang);
                    System.out.println("Berhasil !! sekarang saldo anda menjadi  " + bank.getNasabah(iduser).getTabungan().getSaldo());
                }
               
                else if(pilihan == 3){
                    System.out.print("Berapa saldo yang mau diambil ? : ");
                    jumlahAmbil = masukan.nextInt();
                    status = bank.getNasabah(iduser).getTabungan().bolehKurangiSaldo(jumlahAmbil);
                    if(status){
                        bank.getNasabah(iduser).getTabungan().kurangiSaldo(jumlahAmbil);
                        System.out.println("Berhasil !! sekarang saldo anda menjadi " + bank.getNasabah(iduser).getTabungan().getSaldo());
                    }else
                    System.out.println("Gagal uang anda di saldo terlalu sedikit");
                }
                
                else if(pilihan == 4){
                    try {
                    System.out.print("Rekening yang mau di Transfer : ");
                    String rekeningTrans = masukan.nextLine();
                    System.out.print("ID user yang mau di Transfer : ");
                    int idTrans = masukan.nextInt();
                    masukan.nextLine();

                    valid = bank.getNasabah(idTrans).benarRekening(rekeningTrans);
                    if(valid){
                        System.out.print("Jumlah mau ditransfer : ");
                        jumTrans  = masukan.nextInt();
                        masukan.nextLine();

                        status = bank.getNasabah(iduser).getTabungan().bolehKurangiSaldo(jumTrans);
                        if(status){
                            bank.getNasabah(iduser).getTabungan().kurangiSaldo(jumTrans);
                            bank.getNasabah(idTrans).getTabungan().simpanUang(jumTrans);
                            System.out.println("Berhasil Transfer !! sekarang saldo anda menjadi " + bank.getNasabah(iduser).getTabungan().getSaldo());
                        }else
                        System.out.println("Gagal Transfer uang anda di saldo terlalu sedikit"); 
                    }else
                    System.out.println("Gagal Transfer No Rekening anda Salah");
                    }catch (java.lang.NullPointerException ex) {
                        System.out.println("ID user Anda Salah2");
                    }
                }

                else if(pilihan == 5){
                    System.out.print("Masukkan kode pembayaran : ");
                    jumBayar = masukan.nextInt();
                    masukan.nextLine();
                    temp = 50000;
                    System.out.print("Untuk melakukan pembayaran " + jumBayar + "diperlukan Rp50.000");
                    status = bank.getNasabah(iduser).getTabungan().bolehKurangiSaldo(temp);
                    if(status){
                        bank.getNasabah(iduser).getTabungan().kurangiSaldo(temp);
                        System.out.println("Berhasil Melakukan Pembayaran kode " + jumBayar +  ", sekarang saldo anda menjadi " + bank.getNasabah(iduser).getTabungan().getSaldo());
                    }else
                    System.out.println("Gagal melakukan pembayaran saldo anda terlalu sedikit");
                }

                else{
                    System.out.print("yakin keluar ? (Y/N) ");
                    ulang = masukan.next().charAt(0);
                }
            }while(ulang == 'n' || ulang == 'N');

            System.out.println("");
            System.out.println("Selamat datang KeBank GGS ");
            System.out.println("1. Login");
            System.out.println("2. Register");
            System.out.println("3. keluar");
            System.out.print("Masukkan pilihan anda\t: ");
            pilihanLogin = masukan.nextInt();
            masukan.nextLine();
            
            bank.tambahNasabah(namaAwal, namaAkhir, rekening, iduser);
            bank.getNasabah(iduser).setTabungan(new Tabungan(tabungan));

            if(pilihanLogin == 1){
                System.out.print("Masukkan Rekening : ");
                rekening = masukan.nextLine();
                System.out.print("Masukkan No id user : ");
                iduser = masukan.nextInt();
                System.out.print(iduser);
                bank.getNasabah(iduser).getNamaAwal();
                masukan.nextLine();
                bank.getNasabah(iduser);
            }

            else if(pilihanLogin == 2){
                System.out.print("Masukkan Nama Awal Anda : ");
                namaAwal = masukan.nextLine();
                System.out.print("Masukkan Nama Akhir Anda : ");
                namaAkhir = masukan.nextLine();
                System.out.print("Masukkan Rekening : ");
                rekening = masukan.nextLine();
                System.out.print("Masukkan No id user : ");
                iduser = masukan.nextInt();
                masukan.nextLine();
                bank.tambahNasabah(namaAwal, namaAkhir, rekening,iduser);
                System.out.print("Masukkan Tabungan Awal : ");
                tabungan = masukan.nextInt();
                bank.getNasabah(iduser).setTabungan(new Tabungan(tabungan));
            }
            else { 
                System.exit(0);
            }
            
            do{
                System.out.println("\nSelamat Datang " + bank.getNasabah(iduser).getNamaAwal() + " " + bank.getNasabah(iduser).getNamaAkhir());
                System.out.println("Pilihan Menu Bank");
                System.out.println("1. Cek Saldo ");
                System.out.println("2. Tambah Saldo");
                System.out.println("3. Ambil Saldo ");
                System.out.println("4. Transfer Saldo ");
                System.out.println("5. Pembayaran ");
                System.out.println("6. Keluar ");
                System.out.print("Masukkan Pilihan Menu : ");
                pilihan = masukan.nextInt();
                masukan.nextLine();

                if(pilihan == 1){
                    temp = bank.getNasabah(iduser).getTabungan().getSaldo();
                    System.out.println("Saldo anda adalah (dalam IDR) : " + temp);
                }
                
                else if(pilihan == 2){
                    System.out.print("Berapa jumlah saldo yang mau ditambah  : ");
                    tambahUang = masukan.nextInt();
                    bank.getNasabah(iduser).getTabungan().simpanUang(tambahUang);
                    System.out.println("Berhasil !! sekarang saldo anda menjadi  " + bank.getNasabah(iduser).getTabungan().getSaldo());
                }
               
                else if(pilihan == 3){
                    System.out.print("Berapa saldo yang mau diambil ? : ");
                    jumlahAmbil = masukan.nextInt();
                    status = bank.getNasabah(iduser).getTabungan().bolehKurangiSaldo(jumlahAmbil);
                    if(status){
                        bank.getNasabah(iduser).getTabungan().kurangiSaldo(jumlahAmbil);
                        System.out.println("Berhasil !! sekarang saldo anda menjadi " + bank.getNasabah(iduser).getTabungan().getSaldo());
                    }else
                    System.out.println("Gagal uang anda di saldo terlalu sedikit");
                }
                
                else if(pilihan == 4){
                    try {
                    System.out.print("Rekening yang mau di Transfer : ");
                    String rekeningTrans = masukan.nextLine();
                    System.out.print("ID user yang mau di Transfer : ");
                    int idTrans = masukan.nextInt();
                    masukan.nextLine();

                    valid = bank.getNasabah(idTrans).benarRekening(rekeningTrans);
                    if(valid){
                        System.out.print("Jumlah mau ditransfer : ");
                        jumTrans  = masukan.nextInt();
                        masukan.nextLine();

                        status = bank.getNasabah(iduser).getTabungan().bolehKurangiSaldo(jumTrans);
                        if(status){
                            bank.getNasabah(iduser).getTabungan().kurangiSaldo(jumTrans);
                            bank.getNasabah(idTrans).getTabungan().simpanUang(jumTrans);
                            System.out.println("Berhasil Transfer !! sekarang saldo anda menjadi " + bank.getNasabah(iduser).getTabungan().getSaldo());
                        }else
                        System.out.println("Gagal Transfer uang anda di saldo terlalu sedikit"); 
                    }else
                    System.out.println("Gagal Transfer No Rekening anda Salah");
                    }catch (java.lang.NullPointerException ex) {
                        System.out.println("ID user Anda Salah2");
                    }
                }

                else if(pilihan == 5){
                    System.out.print("Masukkan kode pembayaran : ");
                    jumBayar = masukan.nextInt();
                    masukan.nextLine();
                    temp = 50000;
                    System.out.print("Untuk melakukan pembayaran " + jumBayar + "diperlukan Rp50.000");
                    status = bank.getNasabah(iduser).getTabungan().bolehKurangiSaldo(temp);
                    if(status){
                        bank.getNasabah(iduser).getTabungan().kurangiSaldo(temp);
                        System.out.println("Berhasil Melakukan Pembayaran kode " + jumBayar +  ", sekarang saldo anda menjadi " + bank.getNasabah(iduser).getTabungan().getSaldo());
                    }else
                    System.out.println("Gagal melakukan pembayaran saldo anda terlalu sedikit");
                }

                else{
                    System.out.print("yakin keluar ? (Y/N) ");
                    ulang = masukan.next().charAt(0);
                }
            }while(ulang == 'n' || ulang == 'N');

            }while(pilihanLogout == 'n');
         System.out.print("Terimakasih telah bertransaksi dengan bank GGS :) ");
         masukan.close(); 
    }
}
