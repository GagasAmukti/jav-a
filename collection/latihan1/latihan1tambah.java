package latihan1;
import java.util.*;
public class latihan1tambah {
    public static void main(String[] args) {
        Scanner masuk = new Scanner(System.in);

        System.out.print("inputkan : ");
        String kalimat = masuk.nextLine();
        TreeMap<String, Integer> lat1 = new TreeMap();
        int count = 0;
        
        for(String temp: kalimat.split(" ")){
            if(lat1.containsKey(temp)){

                lat1.put(temp ,lat1.get(temp).intValue()+1);
            }
            else{
                lat1.put(temp, 1);
            }
        }
        System.out.println("Hasil perhitungan : ");
        System.out.println(lat1);
    
        int max = lat1.values().stream().max(Integer::compare).get();
        int min = lat1.values().stream().min(Integer::compare).get();
        
        for (Map.Entry<String, Integer> entry : lat1.entrySet()) {
            int temp = entry.getValue();
            if(temp == max){
                System.out.println("nilai tertinggi "+ entry.getKey() + " = " + entry.getValue());
            }
            else if(temp == min){
                if (count > 0){
                    continue;
                }
                count = 1;
                System.out.println("nilai terendah "+ entry.getKey() + " = " + entry.getValue());        
            }
       }
       System.out.println("key awalan m = " + lat1.tailMap("m"));
    }
}
