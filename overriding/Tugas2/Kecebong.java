package Tugas2;

public class Kecebong extends Katak{
    double panjangEkor;

    Kecebong(int umur, String nama, double panjangEkor){
        super(umur,nama);
        this.panjangEkor = panjangEkor;
    }
    public String INGPOkec(){
        return"\t|" + super.nama + "\t|" + super.umur +
               "\t|" + super.caraBergerak("kecebong") + "\t|\t" + this.panjangEkor + "\t|";
    }
}
