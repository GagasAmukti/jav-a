public class casting {
    public static void main(String[] args) {
        //Narowing Casting
        double doubledua = 69.689;
        int intdua = (int) doubledua;

        System.out.println("Narrowing Casting Source awal (double -> int) : " + doubledua);
        System.out.println("Narrowing Casting Hasil ke Int : " + intdua);
    }
}
