package Tugas1;

public class Superman extends Homosapien implements Flyer{
    public void takeOff(){
        System.out.println("Superman berhasil take off");
    }

    public void land(){
        System.out.println("Superman berhasil landing"); 
    }

    public void fly(){
        System.out.println("Superman terbang dengan kekuatannya");  
    }

    public void leapBuilding(){
        System.out.println("Superman melompati bangunan dengan kekuatannya");  
    }

    public void stopBullet(){
        System.out.println("Superman menghentikan peluru dengan kekuatannya");  
    }
    
    public void eat(){
        System.out.println("Superman sedang makan");
    }

    public void talk(){
       super.talk();
       System.out.println("superman bicara");
    }
}
