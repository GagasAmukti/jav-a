public class Mahasiswa {
    public int nrp = 0;
    public String nama;

    Mahasiswa(int nrp, String nama){
        this.nrp = nrp;
        this.nama = nama;
    }

    public int getNrp(){
        return nrp;
    }

    public String getnama(){
        return nama;
    }
}
