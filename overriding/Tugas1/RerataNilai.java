package Tugas1;
import javax.sound.sampled.Port;

class RerataNilai {
    protected int average(int a, int b){
        return (a + b) / 2;
    }  
    protected double average(Double a, Double b){
        return (a + b) / 2.0;
    }
    protected int average(int a, int b, int c){
        return (a + b + c) / 3;
    }
}
