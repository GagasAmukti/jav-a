package percoban5;

class MountainBike extends Bicycle {
    // the Mountain Bike subclass adds one more field
    public int seatHeight;
    // the MountainBikesubclass has one constructor
    
    public MountainBike(int gear,int speed,int startHeight){
    // invoking base-class(Bicycle) constructor
    super(gear, speed);
    seatHeight = startHeight;
    }
    // the MountainBikesubclass adds one more method
    public void setHeight(int newValue){
    seatHeight= newValue;
    }
    // overriding toString() method
    // of Bicycle to print more info
    @Override
    public String toString(){ 
    return (super.toString() + "\nseatheight is "+ seatHeight);
    }
}
