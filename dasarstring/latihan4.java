public class latihan4 {
    public static void main(String[] args) {
        String selamat = "Selamat Datang di PENS";
        System.out.println("String Awal : " + selamat);

        System.out.print("Konversi menjadi kapital dan tampilkan : ");
        String selamatbutgede = selamat.toUpperCase();
        System.out.print(selamatbutgede);

        System.out.print("\nMenampilkan panjang string : ");
        int length = selamat.length();
        System.out.println(length);

        System.out.print("Menampilkan indeks kata 'PENS' : ");
        int indexx = selamat.indexOf("PENS");
        System.out.println(indexx);
    }
}
