public class Tabungan {
    public int Saldo = 0;

    Tabungan(int Saldo){
        this.Saldo = Saldo;
      }

    public int getSaldo() {
        return Saldo;
    }

    public boolean ambilUang(int Jumlah){
        if(Jumlah > Saldo){
            return false;
        }
        else{
            Saldo -= Jumlah;
            return true;
        }
    }

    public void simpanUang(int Jumlah){
        Saldo += Jumlah;
    }
}
