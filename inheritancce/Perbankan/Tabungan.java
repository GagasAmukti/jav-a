package Perbankan;
class Tabungan {
    protected int saldo;
    protected PengambilanUang tarikSaldo;

    protected Tabungan(){
        this.saldo = 0;
    }

    protected Tabungan(int saldo){
        this.saldo = saldo;
    }

    protected int getSaldo(){
        return saldo;
    }

    protected void setSaldo(int saldo){
        this.saldo = saldo;
    }

    protected void simpanUang(int jumlah){
        this.saldo += jumlah;
    }

    protected PengambilanUang ambilUang(){
        return tarikSaldo;
    }
}
