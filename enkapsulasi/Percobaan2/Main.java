package Percobaan2;
public class Main {
   public static void main(String[] args) {
    Mahasiswa mahasiswa1 = new Mahasiswa();
    Mahasiswa mahasiswa2 = new Mahasiswa("Gagas");
    Mahasiswa mahasiswa3 = new Mahasiswa(69,"Gagas");

    System.out.println(mahasiswa1.getNama() + " " + mahasiswa1.getNrp());
    System.out.println(mahasiswa2.getNama() + " " + mahasiswa2.getNrp());
    System.out.println(mahasiswa3.getNama() + " " + mahasiswa3.getNrp());
   }
}
