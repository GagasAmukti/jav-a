public class promotion {
    public static void main(String[] args) {
        int intsatu = 10;
        double doublesatu = intsatu;

        System.out.println("Widening Casting Source awal (int -> double) : " + intsatu);
        System.out.println("Widening Casting Hasil ke Double : " + doublesatu);
    }
}
