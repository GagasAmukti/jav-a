package Tugas1;

public class Mains {
    public static void main(String[] args) {
        Airplane pesawat = new Airplane();
        Bird merpati = new Bird();
        Superman thor = new Superman();

        pesawat.turnOnEngine(); 
        pesawat.fly();
        System.out.println(" ");

        merpati.fly();
        merpati.eat();
        merpati.buildNest();
        System.out.println(" ");
        
        thor.fly();
        thor.leapBuilding();
        thor.eat();
        
    }
}
