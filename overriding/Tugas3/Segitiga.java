package Tugas3;

class Segitiga extends bangunDatar{
    protected int alas;
    protected int tinggi;

    Segitiga(int alas, int tinggi){
        this.tinggi = tinggi;
        this.alas = alas;
    }

    protected double luas(){
        return 0.5 * this.alas * this.tinggi; 
    }

    protected double keliling(){
        double sisi = Math.sqrt((this.alas * this.alas) + (this.tinggi * this.tinggi));
        return sisi * 3; //dikarenakan saya anggap segitiga siku siku maka haru menemukan sisi dengan 
                        //rumus pythagoras dan kemudian baru mengkali 3 panjang sisi segitiga
    }
}
