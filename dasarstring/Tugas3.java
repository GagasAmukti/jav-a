import java.util.*;
public class Tugas3 {
    public static void main(String[] args) {
        Scanner masukan = new Scanner(System.in);
        String walek = "";

        System.out.print("masukkan teks palindrome yang dicek : ");
        String awal = masukan.nextLine();

        for(int i = awal.length(); i > 0 ; i--){
			String backup = awal.substring(i-1, i);
			walek += backup;
		}
		
		if(awal.equals(walek)){
			System.out.println(awal + " Dibalik " + walek + " adalah palindrom ");
		}else{
			System.out.println(awal + " Dibalik " + walek + " adalah bukan palindrom ");;
		}
        masukan.close();
    }
}
