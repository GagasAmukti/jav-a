package Tugas;

class Nasabah extends Bank{
    private String namaAwal;
    private String namaAkhir;
    private Tabungan tabungan;
    private String noRekening;

    Nasabah(){
        this.namaAkhir = "";
        this.namaAwal = "";
        this.noRekening = "";
    }

    protected Nasabah(String namaAwal, String namaAkhir, String noRekening){
        this.namaAkhir = namaAkhir;
        this.namaAwal = namaAwal;
        this.noRekening = noRekening;
    }

    protected String getNamaAwal(){
        return namaAwal;
    }

    protected String getNamaAkhir(){
        return namaAkhir;
    }

    protected String getRekening(){
        return noRekening;
    }

    protected Tabungan getTabungan(){
        return tabungan;
    }

    protected void setTabungan(Tabungan tabungan){
        this.tabungan = tabungan;
    }

    protected boolean benarRekening(String noRekening){
        return this.noRekening.equals(noRekening);
    }
}
