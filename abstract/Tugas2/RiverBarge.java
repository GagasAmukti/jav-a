package Tugas2;

public class RiverBarge extends Vehicle implements Sailer{
    public void dock(){
        System.out.println("kapal tongkang sedang Docking");
    }

    public void cruise(){
        System.out.println("kapal tongkang Cruising");
    }

    public void turnOnEngine(){
        super.turnOnEngine();
        System.out.println("Mesin kapal tongkang menyala");
    }
}
