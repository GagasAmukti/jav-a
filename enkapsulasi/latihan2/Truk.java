package latihan2;
class Truk {
    private double muatan;
    private double muatanMaks;
    
    public Truk(){
        this.muatanMaks = 0;
        this.muatan = 0;
    }
    
    public Truk(double muatanMaks){
        this.muatanMaks = muatanMaks;
        this.muatan = 0;
    }

    protected double getMuatan() {
        return this.muatan;
    }

    protected double getMuatanMaks(){
        return this.muatanMaks;
    }

    protected boolean tambahMuatan(double berat){
        if((this.muatan + berat) > muatanMaks){
            return false;
        }else{
            this.muatan += berat;
            return true;
        }
    }

    protected double newtsToKilo(double berat){
        return berat * 9.8;
    }

    protected double kiloToNewts(double berat){
        return berat / 9.8;
    }
}
