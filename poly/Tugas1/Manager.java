package Tugas1;
class Manager extends Pegawai{
    protected int tunjangan;

    Manager(String nama, int gaji, int tunjangan){
        super(nama,gaji);
        this.tunjangan = tunjangan;
    }

    public int ingpoGaji(){
        return super.gaji;
    }

    public int ingpoTunjangan(){
        return this.tunjangan;
    }
}
