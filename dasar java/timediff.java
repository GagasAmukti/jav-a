import java.util.*;
import java.text.*;
public class timediff {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        SimpleDateFormat waktu = new SimpleDateFormat("HH:mm");
        
        System.out.print("masukkan Jam & Menit Awal (jam:menit) : ");
        String waktu1= input.next();

        System.out.print("masukkan Jam & Menit Akhir (jam:menit) : ");
        String waktu2= input.next();
        
        Date time1 = waktu.parse(waktu1);
        Date time2 = waktu.parse(waktu2);

        long selisihWaktu = Math.abs(time2.getTime() - time1.getTime());
        long selisihMenit = (selisihWaktu / 1000) / 60;
        System.out.print("Selisih Kedua Waktu dalam Menit : " + (int)selisihMenit);
        input.close();
    }
}
