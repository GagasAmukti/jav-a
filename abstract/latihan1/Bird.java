package latihan1;

public class Bird implements Flyer{
    public void takeOff(){
        System.out.println("Bird berhasil take off");
    }

    public void land(){
        System.out.println("Bird berhasil landing"); 
    }

    public void fly(){
        System.out.println("Bird terbang dengan mengepakkan sayap");  
    }
}
