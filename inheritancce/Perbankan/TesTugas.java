package Perbankan;
public class TesTugas {
    public static void main(String[] args) {
        PengambilanUang tabungan= new PengambilanUang(5000, 1000);
        System.out.println("Uangyang di tabung: 5000");
        System.out.println("Uangyang di proteksi: 1000");
        System.out.println("-----------------------------");
        System.out.println("Uangyang akan diambil: 4500 " + tabungan.ambilUang(4500));
        System.out.println("Saldo sekarang: " + tabungan.getSaldo());
        System.out.println("-----------------------------");
        System.out.println("Uangyang akan diambil: 2500 " +
        tabungan.ambilUang(2500));
        System.out.println("Saldo sekarang: " + tabungan.getSaldo());
    }
}
