import java.util.*;
public class latihansoal {
    public static void main(String[] args) {
        int jum = 0;
        int f1 = 0;
        int f2 = 1;
        Scanner masuk = new Scanner(System.in);
        
        System.out.print("Masukkan panjang baris Fibbonaci : ");
        jum = masuk.nextInt();
        int[] fibbo = new int[jum];
        fibbo[0] = 0;
        fibbo[1] = 1;
        
        for(int i = 2; i < jum; i++){
            fibbo[i] = f1 + f2;
            f1 = f2;
            f2 = fibbo[i];
        }

        System.out.println("Hasil Fibbonaci panjang " + jum + " baris adalah : ");
        for(int j = 0; j < fibbo.length; j++){
            System.out.print(fibbo[j] + " ");
        }
        masuk.close();
    }
}
