package Percobaan2;

public class Fish extends Animal implements Pet {
    private String name;
    
    Fish(String name){
        super(4);
        this.name = name;
    }

    Fish(){
        super(4);
        this.name = " ";
    }
    
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public void walk(){
        System.out.println("hewan ini bergerak berenang bukan berjalan");
    }
    
    public void play(){
        System.out.println("ikan sedang bermain");
    }

    public void eat(){
        System.out.println("ikan sedang makan");
    }
}
