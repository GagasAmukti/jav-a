package Tugas3;

public class Main {
    public static void main(String[] args) {
        bangunDatar bd = new bangunDatar();
        Persegi kotak = new Persegi(5);
        Segitiga segiti = new Segitiga(3, 4);
        lingkaran bunder = new lingkaran(5);

        bd.luas();
        System.out.println("Luas Persegi Empat dengan sisi " + kotak.sisi + " adalah : " + kotak.luas());
        System.out.println("Luas Segitiga dengan alas " + segiti.alas + " dan tinggi " + segiti.tinggi + " adalah : " + segiti.luas());
        System.out.println("Luas Lingkaran dengan jari-jari " + bunder.r + " adalah : " + bunder.luas());

        bd.keliling();
        System.out.println("keliling Persegi Empat dengan sisi " + kotak.sisi + " adalah : " + kotak.keliling());
        //System.out.println("keliling Segitiga dengan alas " + segiti.alas + " tinggi " + segiti.tinggi + " dan sisi" + segiti.sisi + " adalah : " + segiti.keliling());
        System.out.println("Keliling Lingkaran dengan jari-jari " + bunder.r + " adalah : " + bunder.keliling());
    }   
}
