package Tugas;
class AmbilTabungan extends Tabungan{
    protected int saldo;

    @Override
    protected void kurangiSaldo(int jumlah){
        this.saldo = super.getSaldo();
        this.saldo -= jumlah;
        super.setSaldo(this.saldo);
    } 
}
