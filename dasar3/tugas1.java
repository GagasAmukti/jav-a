import java.util.*;
public class tugas1 {
    public static void main(String[] args) {
        float a = 0, b = 0, c = 0, d = 0, x1 = 0, x2 = 0;
        Scanner masukan = new Scanner(System.in);

        System.out.println("Determinan Kuadrat dengan Format  : ax^2 + bx + c");
        System.out.println("Masukkan value a b c dibawah ");
        System.out.print("value a : ");
        a = masukan.nextInt();
        System.out.print("Value b : ");
        b = masukan.nextInt();
        System.out.print("Value c : ");
        c = masukan.nextInt();
        d = (float) Math.pow(b, 2) - 4 * a * c;
        System.out.printf("Value D : %f\n", d);
        
        if(d == 0){
            x1 = -b/2*a;
            System.out.printf("Hasil akar-akarnya kembar yaitu %f",x1);
        }
        else if(d > 0){
            x1 = (-b + (float)Math.pow(d, 0.5)) / 2 * a;
            x2 = (-b - (float)Math.pow(d, 0.5)) / 2 * a;
            System.out.printf("Hasil akar-akarnya real yang berlainan, yaitu %f dan %f",x1 ,x2 );
        }
        else if(d < 0){
            x1 = -b / (2 * a) + (float)Math.pow(-d,0.5) / 2 * a;
            x2 = -b /(2 * a) - (float)Math.pow(-d,0.5) / 2 * a;
            System.out.printf("Hasil akar-akarnya imaginer yang berlainan, yaitu %f dan %f",x1 ,x2 );
        }else{
            System.out.printf("Ya Ndak Tau Kok Tanyak Saya");
        }
        masukan.close();
    }
}
