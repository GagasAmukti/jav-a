import java.util.*;
public class tugas2 {
    public static void main(String[] args) {
        int tahun = 0;
        boolean kabisat;
        char pilihan;
        Scanner masuk = new Scanner(System.in);
        
        do{
            System.out.print("Masukkan tahun yang di cek : ");
            tahun = masuk.nextInt();
          
            if (tahun < 1900 || tahun > 2005) {
              System.out.println("Mohon maaf tahun minimal 1900 dan maks 2005");
            }else{
              if (tahun % 4 == 0) {
                  if (tahun % 100 == 0) {
                    if (tahun % 400 == 0){
                      kabisat = true;
                    }else{
                      kabisat = false;
                    }
                  }else{
                    kabisat = true;
                  }
                }else{
                  kabisat = false;
                }
                if(kabisat){
                  System.out.print("Tahun tersebut kabisat\n");
                }else{
                  System.out.print("Tahun tersebut bukan kabisat\n");
                }
              }   
              System.out.print("Mau Ulangi ? (Y/N) : ");
              pilihan = masuk.next().charAt(0);
          }while(pilihan == 'y' || pilihan == 'Y');
    masuk.close();
  }
}
