package latihan4;
public class Main extends Pegawai{
    public static void main(String[] args) {
        Pegawai pgw = new Pegawai();
        Pegawai mgr = new Manager();
        Pegawai kri = new Kurir();

        pgw.pegawaiMethod();
        mgr.managerMethod();
        pgw.managerMethod();
        kri.kurirMethod();
        pgw.kurirMethod();
    }  
}
