/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package finalprojectoop;

import finalprojectoop.Bank;

/**
 *
 * @author amukt
 */
class Tabungan extends Bank{
    private int saldo;
    private int minimum = 50000;

    protected Tabungan(){
        this.saldo = 0;
    }
    
    protected Tabungan(int saldo){
        this.saldo = saldo;
    }

    protected int getSaldo(){
        return saldo;
    }

    protected void simpanUang(int jumlah){
        this.saldo += jumlah;
    }

    protected void kurangiSaldo(int jumlah){
        this.saldo -= jumlah;
    }

    protected boolean bolehKurangiSaldo(int jumlah){
        if(jumlah > saldo - minimum){
            return false;
        }else{
            return true;
        }
    }    
}
