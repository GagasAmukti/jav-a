/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package finalprojectoop;

import BANK.Tabungan;
import finalprojectoop.Bank;

/**
 *
 * @author amukt
 */
class Nasabah extends Bank{
    private String namaAwal;
    private String namaAkhir;
    private Tabungan tabungan;
    private String noRekening;

    public Nasabah(String namaAwal, String namaAkhir, String noRekening){
        this.namaAkhir = namaAkhir;
        this.namaAwal = namaAwal;
        this.noRekening = noRekening;
    }

    public String getNamaAwal(){
        return namaAwal;
    }

    public String getNamaAkhir(){
        return namaAkhir;
    }

    public String getRekening(){
        return noRekening;
    }

    public Tabungan getTabungan(){
        return tabungan;
    }

    public void setTabungan(Tabungan tabungan){
        this.tabungan = tabungan;
    }

    protected boolean benarRekening(String noRekening){
        return this.noRekening.equals(noRekening);
    }
}
