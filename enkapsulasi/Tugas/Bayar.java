package Tugas;

class Bayar extends Tabungan{
    private int saldo;
    private int kodepemBayaran;

    protected void kurangiSaldo(int jumlah, int kodepembayaran){
        this.kodepemBayaran = kodepembayaran;
        saldo = super.getSaldo();
        saldo -= jumlah;
        super.setSaldo(saldo);
    }
}
