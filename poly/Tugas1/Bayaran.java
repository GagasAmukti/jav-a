package Tugas1;
public class Bayaran extends Pegawai{
    public int hitungbayaran(Pegawai peg){
        int uang = peg.ingpoGaji();
        if(peg instanceof Manager){
            uang += ((Manager) peg).ingpoTunjangan();
        }
        else if(peg instanceof Programmer){
            uang += ((Programmer) peg).ingpoBonus();
        }
        return uang;
    }
    public static void main(String[] args) {
        Manager man = new Manager("Agus", 800, 50);
        Programmer prog = new Programmer("Budi", 600, 30);
        Bayaran hr = new Bayaran();

        System.out.println("Bayaran untuk Manager : " + hr.hitungbayaran(man));
        System.out.println("Bayaran untuk Programmer : " + hr.hitungbayaran(prog));
    }
}
