package latihan1;
class Kalender {
    private int tanggal= 0, bulan = 0, tahun = 0;

    public Kalender(){
        this.tanggal = 1;
        this.bulan = 1;
        this.tahun = 2000;
    }
    
    public Kalender(int tanggal){
        this.tanggal = tanggal;
        this.bulan = 1;
        this.tahun = 2000;
    }

    public Kalender(int bulan,int tahun){
        this.tanggal = 1;
        this.bulan = bulan;
        this.tahun = tahun;
    }

    public Kalender(int tanggal,int bulan,int tahun){
        this.tanggal = tanggal;   
        this.bulan = bulan;
        this.tahun = tahun;
    }

    protected int getTanggal(){
        return this.tanggal;
    }
    protected int getBulan(){
        return this.bulan;
    }
    protected int getTahun(){
        return this.tahun;
    }
    protected void setTanggal(int tanggal){
        this.tanggal = tanggal;
    }  
    protected void setBulan(int bulan){
        this.bulan = bulan;
    }   
    protected void setTahun(int tahun){
        this.tahun = tahun;
    }
}
