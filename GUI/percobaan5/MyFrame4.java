package percobaan5;
import javax.swing.*; 
import java.awt.*; 
public class MyFrame4 extends JFrame{
    private Container container = new Container();
    private JButton tombol = new JButton ("Tombol");
    private JLabel tulisan = new JLabel ("Tulisan"); 
    MyFrame4(){
        super ("Demo FlowLayout"); 

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(250,80);
        setLocation(100,100);
        container = getContentPane(); 
        container.setLayout (new FlowLayout());

        container.add (tombol); 
        container.add (tulisan); 
        show();
    }
}
