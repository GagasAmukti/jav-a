    package latihan1;

    public class Mains {
        public static void main(String[] args) {
            Airplane pesawat = new Airplane();
            Bird merpati = new Bird();
            Superman thor = new Superman();

            pesawat.fly();
            merpati.fly();
            thor.fly();
        }
    }
