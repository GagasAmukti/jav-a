public class analisabyteshift {
    public static void main(String[] args) {
        byte a = -1;
        String before, after;
        
        System.out.println("Sebelum Shifting");
        before = Integer.toBinaryString(a);
        System.out.println(before);
        System.out.println(a);

        System.out.println("Sesudah Shifting (a >>> 2)");
        a=(byte) (a >>> 2);
        after = Integer.toBinaryString(a);
        System.out.println(after);
        System.out.println(a);
    }
}
