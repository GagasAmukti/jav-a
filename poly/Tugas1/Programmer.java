package Tugas1;
class Programmer extends Pegawai{
    protected int bonus;

    Programmer(String nama, int gaji, int bonus){
        super(nama,gaji);
        this.bonus = bonus;
    }

    public int ingpoGaji(){
        return super.gaji;
    }

    public int ingpoBonus(){
        return this.bonus;
    }
}
