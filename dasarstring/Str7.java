public class Str7 {
    public static void main(String[] args) {
        String s = "Welcome to Java 2";
        int x1, x2, x3, x4;
        // returns 0, first position
        x1 = s.indexOf("W");
        x2 = s.indexOf("J"); // returns 11
        x3 = s.indexOf("2"); // returns 16
        // returns -1 because it’s case-sensitive
        x4 = s.indexOf("java");

        System.out.println(x1);
        System.out.println(x2);
        System.out.println(x3);
        System.out.println(x4);
    }
}
