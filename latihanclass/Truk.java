public class Truk {
    public double muatan = 0;
    public double muatanMax = 0;
 
    Truk(double muatanMax){
        this.muatanMax = muatanMax;
    }

    public double getMuatan(){
        return muatan;
    }

    public double getMuatanMaks(){
        return muatanMax;
    }
    
    public void tambahMuatan(double berat){
        this.muatan += berat;
    }
}
