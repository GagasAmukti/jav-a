package Tugas3;

class lingkaran extends bangunDatar{
    protected int r;

    lingkaran(int r){
        this.r = r;
    }

    protected double luas(){
        return 3.14 * r * r;
    }

    protected double keliling(){
        return 2 * 3.14 * r ;
    }
}
