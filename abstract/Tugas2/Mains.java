package Tugas2;

public class Mains {
    public static void main(String[] args) {
        Airplane pesawat = new Airplane();
        RiverBarge kpltongkang = new RiverBarge();
        Seaplane pesawatlaut = new Seaplane();
        Helicopter heli = new Helicopter();

        pesawat.turnOnEngine(); 
        pesawat.takeOff();
        pesawat.fly();
        System.out.println(" ");

        kpltongkang.turnOnEngine();
        kpltongkang.cruise();
        System.out.println(" ");

        pesawatlaut.dock();
        pesawatlaut.cruise();
        System.out.println(" ");

        heli.takeOff();
    }
}
