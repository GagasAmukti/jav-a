package latihan4;

public class main {
    public static void main(String[] args) {
        System.out.println("Memasukkan identitas dosen1 : Agus");
        Dosen dosen1 = new Dosen("Agus");
        System.out.println("Memasukkan identitas dosen2 : Budi, NIP. 1458");
        Dosen dosen2 = new Dosen("Budi", 1458);
        System.out.println("Memasukkan identitas dosen3 : Iwan, NIP. 1215, umur 47");
        Dosen dosen3 = new Dosen("Iwan", 1215, 47);
        System.out.println("\n" + dosen1.INGPO());
        System.out.println("\n" +dosen2.INGPO());
        System.out.println("\n" +dosen3.INGPO() + "\n");
        
    }
}
