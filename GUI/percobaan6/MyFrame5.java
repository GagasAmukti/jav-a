package percobaan6;
import javax.swing.*; 
import java.awt.*; 
public class MyFrame5 extends JFrame{
    private Container container = new Container();
    private JButton tombol = new JButton ("Tombol");
    private JLabel tulisan = new JLabel ("Tulisan"); 

    MyFrame5(){
        super ("GridLayout 1-baris 2-kolom");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(250,80);
        setLocation(100,100);
        container = getContentPane();
        container.setLayout (new GridLayout(1,2));
        container.add (tulisan);
        container.add (tombol);
        show();
    }
}
