package percobaan7;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
public class IterateValuesOfHashMapExample {
    public static void main(String[] args) {
    //create HashMapobject
    HashMap hMap= new HashMap();
    //add key value pairs to HashMap
    hMap.put("1","One");
    hMap.put("2","Two");
    hMap.put("3","Three");
    Collection c = hMap.values();
    //obtain an Iterator for Collection
    Iterator itr= c.iterator();
    //iterate through HashMapvalues iterator
    while(itr.hasNext()){
        System.out.println(itr.next());
        }
    }
}
