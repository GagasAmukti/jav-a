package latihan1;
import java.util.*;
public class latihan1 {
    public static void main(String[] args) {
        TreeMap<String, Integer> lat1 = new TreeMap();
        Scanner masuk = new Scanner(System.in);

        System.out.print("inputkan : ");
        String kalimat = masuk.nextLine();
        
      
        for(String temp: kalimat.split(" ")){
            if(lat1.containsKey(temp)){

                lat1.put(temp ,lat1.get(temp).intValue()+1);
            }
            else{
                lat1.put(temp, 1);
            }
        }
        masuk.close();
        System.out.println("Hasil perhitungan : ");
        System.out.println(lat1);
    }
}
