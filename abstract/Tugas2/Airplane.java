package Tugas2;

public class Airplane extends Vehicle implements Flyer{
    public void takeOff(){
        System.out.println("Aircraft berhasil take off");
    }

    public void land(){
        System.out.println("Pesawat berhasil landing"); 
    }

    public void fly(){
        System.out.println("Airplane terbang menggunakan sayap dan dorongan mesin jet");  
    }
    
    public void turnOnEngine(){
        super.turnOnEngine();
        System.out.println("Mesin pesawat menyala");
    }
}
