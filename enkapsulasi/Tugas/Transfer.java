package Tugas;

class Transfer extends Tabungan{
    protected boolean status;
    protected Bank bank;
    protected int idUser;
    protected int idTrans;
    protected int saldo;
    
    protected void kurangiSaldo(boolean status, int iduser, int idTrans, int jumTrans, Bank bank){
        try {
            if(status){
                bank.getNasabah(iduser).getTabungan().kurangiSaldo(jumTrans);
                bank.getNasabah(idTrans).getTabungan().simpanUang(jumTrans);
                System.out.println("Berhasil Transfer !! sekarang saldo anda menjadi " + bank.getNasabah(iduser).getTabungan().getSaldo());
            }else
            System.out.println("Gagal Transfer uang anda di saldo terlalu sedikit"); 
        }catch (java.lang.NullPointerException ex) {
            System.out.println("Ada Kegagalan");
        }
    }
}
