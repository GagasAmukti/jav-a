package Tugas;
class Tabungan extends Nasabah{
    protected int saldo;
    private int minimum = 50000;
    AmbilTabungan ambiltabungan;
    Transfer transfer;
    Bayar bayar;

    protected Tabungan(){
        this.saldo = 0;
    }
    
    protected Tabungan(int saldo){
        this.saldo = saldo;
    }

    protected int getSaldo(){
        return saldo;
    }

    protected void setSaldo(int saldo){
        this.saldo = saldo;
    }

    public void setAmbilTabungan(AmbilTabungan ambiltabungan){
        this.ambiltabungan = ambiltabungan;
    }

    public void setTransfer(Transfer transfer){
        this.transfer = transfer;
    }

    public void setBayar(Bayar bayar){
        this.bayar = bayar;
    }

    protected AmbilTabungan getAmbilTabungan(){
        return ambiltabungan;
    }

    protected Transfer getTransfer(){
        return transfer;
    }

    protected Bayar getBayar(){
        return bayar;
    }

    protected void simpanUang(int jumlah){
        this.saldo += jumlah;
    }

    protected void kurangiSaldo(int jumlah){
        this.saldo -= jumlah;
    }

    protected boolean bolehKurangiSaldo(int jumlah){
        if(jumlah > saldo - minimum){
            return false;
        }else{
            return true;
        }
    }    
}
