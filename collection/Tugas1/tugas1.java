package Tugas1;
import java.util.*;
public class tugas1 {
    public static void main(String[] args) {
        TreeMap<String, TreeMap<String, String>> pulau = new TreeMap();
        TreeMap<String, String> prov = new TreeMap();
        System.out.println("MANOK MANOK MANOK ");
        System.out.println("MANOK MANOK MANOK ");
        System.out.println("MANOK MANOK MANOK ");
        System.out.println("MANOK MANOK MANOK ");
        System.out.println("MANOK MANOK MANOK ");

        pulau.put("Sumatera", new TreeMap<String, String>());
        TreeMap<String, String> sumatera = pulau.get("Sumatera");
        sumatera.put("Nanggroe Aceh Darusallam", "Aceh");
        sumatera.put("Sumatera Utara", "Medan");
        sumatera.put("Sumatera Barat", "Padang");
        sumatera.put("Sumatra Selatan", "Palembang");
        sumatera.put("Riau", "Pekan Baru");
        sumatera.put("Kepulauan Riau", "Tanjung Pinang");
        sumatera.put("Jambi", "Jambi");
        sumatera.put("Bangka Belitung", "Pangkal Pinang");
        sumatera.put("Bengkulu", "Bengkulu");
        sumatera.put("Lampung", "Bandar Lampung");

        pulau.put("Jawa", new TreeMap<String, String>());
        TreeMap<String, String> jawi = pulau.get("Jawa");
        jawi.put("DKI Jakarta", "Jakarta");
        jawi.put("Banten", "Serang");
        jawi.put("Daerah Istimewa Yogyakarta", "Yogyakarta");
        jawi.put("Jawa Timur", "Surabaya");
        jawi.put("Jawa Barat", "Bandung");
        jawi.put("Jawa Tengah", "Semarang");

        pulau.put("Sulawesi", new TreeMap<String, String>());
        TreeMap<String, String> Sulawesi = pulau.get("Sulawesi");
        Sulawesi.put("Gorontalo", "Gorontalo");
        Sulawesi.put("Sulawesi Barat", "Mamuju");
        Sulawesi.put("Sulawesi Tengah", "Palu");
        Sulawesi.put("Sulawesi Utara", "Manado");
        Sulawesi.put("Sulawesi Tenggara", "Kendari");
        Sulawesi.put("Sulawesi Selatan", "Makassar");

        System.out.println("provinsi yang ada di Jawa adalah :");
        System.out.println(pulau.get("Jawa"));

        System.out.println("\nprovinsi yang ada di Sumatera adalah : ");
        System.out.println(pulau.get("Sumatera"));

        System.out.println("\nprovinsi yang berawalan S : ");
        for(TreeMap<String, String> cari:pulau.values()){
            SortedMap<String, String> hasil = cari.tailMap("S");
            System.out.println(hasil);
        }
        
    }
}
