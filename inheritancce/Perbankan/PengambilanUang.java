package Perbankan;
class PengambilanUang extends Tabungan{
    private int proteksi;
    
    PengambilanUang(int saldo){
        super(saldo);
    }

    PengambilanUang(int saldo, int tingkatBunga){
        super(saldo);
        this.proteksi = tingkatBunga;
    }
    
    public boolean ambilUang(int jumlah){
        int temp = super.saldo;
        
        if((temp - jumlah) < proteksi){
            return false;
        }else{
            temp -= jumlah;
            super.setSaldo(temp);
            return true;
        }
    }
}
