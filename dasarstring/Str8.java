public class Str8 {
    public static void main(String[] args) {
        // s1 is set to “red”
        String s1 = "wired".substring(2,5);
        // s2 is set to “str”
        String s2 = "substring".substring(3,5);
        System.out.println(s1);
        System.out.println(s2);
    }
}
