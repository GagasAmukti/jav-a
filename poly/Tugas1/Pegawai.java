package Tugas1;
class Pegawai {
    protected String nama;
    protected int gaji;

    Pegawai(){
        this.nama = "kosong";
        this.gaji = 0;
    }

    Pegawai(String nama, int gaji){
        this.gaji = gaji;
        this.nama = nama;
    }

    protected int ingpoGaji(){
        return this.gaji;
    }   
}
