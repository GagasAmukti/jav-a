package Tugas2;

public class Seaplane extends Airplane implements Sailer{
    @Override
    public void dock(){
        System.out.println("Pesawat laut Docking");
    }
    @Override
    public void cruise(){
        System.out.println("Pesawat laut Cruising");
    }
}
