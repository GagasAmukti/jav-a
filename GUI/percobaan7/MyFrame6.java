package percobaan7;

import javax.swing.*; 
import java.awt.*; 
public class MyFrame6 extends JFrame{
    private Container container = new Container();
    private JButton tombolUtara = new JButton ("NORTH");
    private JButton tombolSelatan = new JButton ("SOUTH");
    private JButton tombolTimur = new JButton ("EAST");
    private JButton tombolBarat = new JButton ("WEST");
    private JButton tombolTengah = new JButton ("CENTER"); 

    MyFrame6(){
        super ("Demo Border Layout");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize (250,130);
        setLocation(100,100);
        
        container = getContentPane();
        container.setLayout (new BorderLayout());

        container.add (tombolUtara, BorderLayout.NORTH);
        container.add (tombolSelatan, BorderLayout.SOUTH);
        container.add (tombolTimur, BorderLayout.EAST);
        container.add (tombolBarat, BorderLayout.WEST);
        container.add (tombolTengah, BorderLayout.CENTER);
        show();
    }
}
