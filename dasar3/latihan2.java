import java.util.*;
public class latihan2 {
    public static void main(String[] args) {
        Scanner masuk = new Scanner(System.in);
        int faktor = 0;
        int hasilFak = 1;
        
        System.out.print("Masukkan Bilangan akan di faktorialkan : ");
        faktor = masuk.nextInt();
        
        System.out.println("n\tn!");
        System.out.println("-----------");
        for(int i = 1; i <= faktor; i++){
            hasilFak = hasilFak * i;
            System.out.println(i + "\t" + hasilFak); 
        }
        System.out.println("-----------");
        masuk.close();
    }
}
