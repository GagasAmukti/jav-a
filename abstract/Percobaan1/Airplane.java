package Percobaan1;

public class Airplane implements Flyer{
    @Override
    public void takeOff(){
        //do a takeoff checklist
        //acclerate until V1 Speed and pull nose up 
        //landing gear up, autopilot on, adjust trim 
        System.out.println("Pesawat berhasil take off");
    }

    @Override
    public void land(){
        //do a landing checklist, contact ATC to permision to landing 
        //lower power, extends flap and lower landing gear
        //before touchdown flare a bit and touchdown apply brake until stop
        System.out.println("Pesawat berhasil landing"); 
    }

    @Override
    public void fly(){
        //keep autopilot on and stay in fvr route
        System.out.println("Pesawat telah terbang");  
    }
    
}
