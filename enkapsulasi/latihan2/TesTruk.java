package latihan2;
class TesTruk extends Truk{
    public static void main(String[] args) {
        boolean status;

        Truk truk = new Truk(900);
        System.out.println("Muatan Maksimal = " + truk.getMuatanMaks());
        status = truk.tambahMuatan(500.0);
        System.out.println("Tambah Muatan : 500");
        if(status)
            System.out.println("OK");
        else
            System.out.println("Gagal");

        status = truk.tambahMuatan(300.0);
        System.out.println("Tambah Muatan : 300");
        if(status)
            System.out.println("OK");
        else
            System.out.println("Gagal");
        
        status = truk.tambahMuatan(150.0);
        System.out.println("Tambah Muatan : 150");
        if(status)
            System.out.println("OK");
        else
            System.out.println("Gagal");
        
        status = truk.tambahMuatan(50.0);
        System.out.println("Tambah Muatan : 50");
        if(status)
            System.out.println("OK");
        else
            System.out.println("Gagal");
        
            System.out.println("Muatan Sekarang = " + truk.getMuatan());
    }
}
